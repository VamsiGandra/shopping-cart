import { combineReducers } from "redux";
import modal_open from "./modalReducer";
import cart from "./cartReducer";

const reducers = combineReducers({
  modal_open,
  cart
});

export default reducers;
