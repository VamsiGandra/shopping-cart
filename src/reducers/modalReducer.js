import * as types from '../actions/actions'

const initialState = {
    modal_open: false
}

export default function modal_open(state = initialState, action) {
    
    switch (action.type) {
        case types.MODAL_OPEN:
            return {
                modal_open: true
            }
        case types.MODAL_CLOSE:
            return {
                modal_open: false
            }
        default:
            return state
    }
}
