import * as types from "../actions/actions";

const initState = {
  items: [
    { index: 1, name: "Sledgehammer", price: 127.75 },
    { index: 2, name: "Axe", price: 190.5 },
    { index: 3, name: "Bandsaw", price: 562.13 },
    { index: 4, name: "Chisel", price: 12.9 },
    { index: 5, name: "Hacksaw", price: 18.45 }
  ],
  addedItems: []
};

export default function cart(state = initState, action) {
  if (action.type === types.ADD_TO_CART) {
    let addedItem = state.items.find(item => item.index === action.index);
    let existed_item = state.addedItems.find(
      item => action.index === item.index
    );
    if (existed_item) {
      addedItem.quantity += 1;
      return {
        ...state
      };
    } else {
      addedItem.quantity = 1;
      return {
        ...state,
        addedItems: [...state.addedItems, addedItem]
      };
    }
  } else if (action.type === types.REMOVE_ITEM) {
    let new_items = state.addedItems.filter(
      item => action.index !== item.index
    );
    return {
      ...state,
      addedItems: new_items
    };
  } else if (action.type === types.ADD_QUANTITY) {
    let addedItem = state.items.find(item => item.index === action.index);
    let newAddedItem = state.addedItems.map(item => {
      return { ...item };
    });
    let add_Item = newAddedItem.find(item => item.index === action.index);
    add_Item.quantity += 1;
    addedItem.quantity += 1;
    return {
      ...state,
      addedItems: newAddedItem
    };
  } else if (action.type === types.SUB_QUANTITY) {
    let addedItem = state.items.find(item => item.index === action.index);
    if (addedItem.quantity === 1) {
      let new_items = state.addedItems.filter(
        item => item.index !== action.index
      );
      return {
        ...state,
        addedItems: new_items
      };
    } else {
      let newAddedItem = state.addedItems.map(item => {
        return { ...item };
      });
      let add_Item = newAddedItem.find(item => item.index === action.index);
      add_Item.quantity -= 1;
      addedItem.quantity -= 1;
      return {
        ...state,
        addedItems: newAddedItem
      };
    }
  } else {
    return state;
  }
}
