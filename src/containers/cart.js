import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import ExposurePlus1 from "@material-ui/icons/ExposurePlus1";
import ExposureNeg1 from "@material-ui/icons/ExposureNeg1";
import DeleteIcon from "@material-ui/icons/Delete";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";

import * as actions from "../actions";

const styles = theme => ({
  modal_card: {
    margin: "10px",
    padding: "10px",
    boxShadow: "2px 2px 5px gray"
  },
  button: {
    marginLeft: "20px",
    marginTop: "5px"
  },
  label: {
    marginLeft: theme.spacing(7),
    fontSize: theme.spacing(2),
    fontWeight: "bold"
  },
  title: {
    fontSize: "20px"
  },
  total: {
    position: "absolute",
    marginLeft: "-250px",
    fontWeight: "bold"
  }
});

class Cart extends Component {
  constructor(props) {
    super(props);
    this.modalClose = this.modalClose.bind(this);
  }

  modalClose() {
    this.props.modal_close();
  }

  handleAddQuantity(index) {
    this.props.addQuantity(index);
  }

  handleSubQuantity(index) {
    this.props.subQuantity(index);
  }

  handleRemoveItem(index) {
    this.props.removeItem(index);
  }

  render() {
    const { classes } = this.props;
    let all_total = 0;
    for (let i = 0; i < this.props.add_cart.length; i++) {
      all_total +=
        this.props.add_cart[i].quantity * this.props.add_cart[i].price;
    }
    let all_total_price = all_total.toFixed(2);
    let addedItems = this.props.add_cart.length ? (
      this.props.add_cart.map(item => {
        return (
          <Card className={classes.modal_card} key={item.index}>
            <Grid>
              <Typography className={classes.label}>
                Name: {item.name}
              </Typography>
              <Typography className={classes.label}>
                Price: {item.price}$
              </Typography>
              <Typography className={classes.label}>
                Quantity: {item.quantity}
              </Typography>
              <Typography className={classes.label}>
                Total: {(item.quantity * item.price).toFixed(2)}$
              </Typography>
            </Grid>
            <Grid>
              <Fab
                className={classes.button}
                onClick={() => this.handleAddQuantity(item.index)}
              >
                <ExposurePlus1 />
              </Fab>
              <Fab
                className={classes.button}
                onClick={() => this.handleSubQuantity(item.index)}
              >
                <ExposureNeg1 />
              </Fab>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                onClick={() => this.handleRemoveItem(item.index)}
              >
                Delete
                <DeleteIcon />
              </Button>
            </Grid>
          </Card>
        );
      })
    ) : (
      <div>Nothing</div>
    );
    return (
      <Dialog
        open={this.props.modal_open}
        maxWidth="xs"
        fullWidth={true}
        className={classes.dialog}
      >
        <DialogTitle>Cart</DialogTitle>
        <DialogContent>{addedItems}</DialogContent>
        <DialogActions>
          <Typography className={classes.total}>
            All Total: {all_total_price}$
          </Typography>
          <Button onClick={this.modalClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    modal_open: state.modal_open.modal_open,
    add_cart: state.cart.addedItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    modal_close: () => {
      dispatch(actions.modal_close());
    },
    addQuantity: index => {
      dispatch(actions.addQuantity(index));
    },
    subQuantity: index => {
      dispatch(actions.subQuantity(index));
    },
    removeItem: index => {
      dispatch(actions.removeItem(index));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Cart));
