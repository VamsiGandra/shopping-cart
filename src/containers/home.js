import React, { Component } from "react";
import Header from "./header";
import Product from "./product";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";

const styles = theme => ({
  title: {
    fontWeight: "bold",
    fontSize: "50px",
    textAlign: "center"
  }
});

class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Header />
        <Typography className={classes.title}>Products</Typography>
        {this.props.items.map(item => (
          <Product key={item.index} {...item} />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.cart.items
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Home));
