import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { connect } from "react-redux";
import * as actions from "../actions";

const styles = theme => ({
  cart: {
    width: "250px",
    height: "60px",
    margin: "40px 100px 40px 100px",
    float: "left",
    padding: "40px",
    boxShadow: "2px 2px 7px gray"
  },
  name: {
    fontSize: "20px"
  },
  price: {
    fontSize: "20px",
    fontWeight: "bold"
  },
  addicon: {
    position: "absolute",
    zIndex: "2",
    marginLeft: "170px",
    marginTop: "10px"
  }
});

class Product extends Component {
  handleClick = index => {
    this.props.addToCart(index);
  };

  render() {
    const { classes } = this.props;
    // console.log(this.props)
    return (
      <Card className={classes.cart}>
        <Typography className={classes.name}>{this.props.name}</Typography>
        <Typography className={classes.price}>{this.props.price}</Typography>
        <Fab
          color="primary"
          aria-label="add"
          className={classes.addicon}
          onClick={() => this.handleClick(this.props.index)}
        >
          <AddIcon />
        </Fab>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: index => {
      dispatch(actions.addToCart(index));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Product));
