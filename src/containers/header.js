import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";
import IconButton from "@material-ui/core/IconButton";
import Cart from "./cart";
import * as actions from "../actions";
import { connect } from "react-redux";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  header: {
    height: theme.spacing(10)
  },
  cart: {
    position: "absolute",
    right: theme.spacing(5),
    fontSize: "200%"
  },
  title: {
    fontSize: "50px"
  }
});

class Header extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.modal_open();
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.header}>
          <Toolbar variant="dense">
            <Typography variant="h6" color="inherit" className={classes.title}>
              Shopping Cart
            </Typography>
            <IconButton
              color="default"
              aria-label="add to shopping cart"
              onClick={this.handleClick}
              className={classes.cart}
            >
              <ShoppingBasket />
            </IconButton>
          </Toolbar>
          <Cart />
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    modal_open: () => {
      dispatch(actions.modal_open());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Header));
