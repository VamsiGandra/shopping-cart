import * as types from './actions'

export function modal_open() {
    return {
        type: types.MODAL_OPEN
    }
}

export function modal_close() {
    return {
        type: types.MODAL_CLOSE
    }
}

export function addToCart(index, name, price) {
    return {
        type: types.ADD_TO_CART,
        index,
        name,
        price,
    }
}

export function addQuantity(index) {
    return {
        type:types.ADD_QUANTITY,
        index
    }
}

export function subQuantity(index) {
    return {
        type:types.SUB_QUANTITY,
        index
    }
}

export function removeItem(index) {
    return {
        type: types.REMOVE_ITEM,
        index
    }
}